// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Spike_4GameModeBase.generated.h"

/**
 *
 */
UCLASS()
class SPIKE_4_API ASpike_4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
		void GameOver();
};
