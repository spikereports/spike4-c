// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Spike_4.h"
#include "MyPawn.h"

// Sets default values
AMyPawn::AMyPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set this pawn to be controlled by the lowest-numbered player
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Create a dummy root component we can attach things to.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	// Create a camera and a visible object
	OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OurVisibleComponent"));

	// Attach our camera and visible object to our root component. Offset and rotate the camera.
	OurCamera->SetupAttachment(RootComponent);
	OurCamera->SetRelativeLocation(FVector(-250.0f, 0.0f, 350.0f));
	OurCamera->SetRelativeRotation(FRotator(-50.0f, 0.0f, 0.0f));
	OurVisibleComponent->SetupAttachment(RootComponent);

	bUseControllerRotationYaw = true;

	
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Handle movement based on our "MoveX" and "MoveY" axes
	UpdateVelocity();

	if (!CurrentVelocity.IsZero())
	{
		FVector NewLocation = GetActorLocation() + (CurrentVelocity * DeltaTime);
		SetActorLocation(NewLocation);
	}

}



// Moves the player forwards or backwards along the local x axis
// Amount moved is based upon the Sprint argument
void AMyPawn::Move_XAxis(float AxisValue)
{
	// 1. Convert our Axis Value into a desired Velocity
	// 1a. Make sure Axis Value was correctly between -1 and 1
	auto NewVelocityX = FMath::Clamp(AxisValue, -1.0f, 1.0f);

	// 1b. Scale our New Velocity from [-1.0, 1.0] -> [-100.0, 100.0].
	NewVelocityX *= 100;

	// 1c. If we are running, increase the velocity even further! [-500, 500]!
	if (IsRunning)
	{
		NewVelocityX *= 5;
	}

	// 2. store for later use:
	DesiredForwardSpeed = NewVelocityX;
	
}


// Moves the player forwards or backwards along the local x axis
// Amount moved is based upon the Sprint argument
void AMyPawn::Move_YAxis(float AxisValue)
{
	// 1. Convert our Axis Value into a desired Velocity
	// 1a. Make sure Axis Value was correctly between -1 and 1
	auto NewVelocityY = FMath::Clamp(AxisValue, -1.0f, 1.0f);

	// Scale the new velocity 
	NewVelocityY *= 150;

	// Increase the current velocity for sprinting
	if (IsRunning)
	{
		NewVelocityY *= 5;
	}

	DesiredRightSpeed = NewVelocityY;
	
}


// bool that states whether or not the player is running.
void AMyPawn::Sprint(bool NewRunning)
{
	this->IsRunning = NewRunning;
}

void AMyPawn::UpdateVelocity()
{
	FVector NewVelocity{ DesiredForwardSpeed, DesiredRightSpeed, 0 };

	
	// Prevent people from moving faster when moving diagonally
	//NewVelocity = NewVelocity.GetClampedToMaxSize(1.0f);

	CurrentVelocity = GetControlRotation().RotateVector(NewVelocity);

}
