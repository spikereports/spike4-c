// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GameFramework/Pawn.h"
#include "MyPawn.generated.h"

UCLASS()
class SPIKE_4_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values
	AMyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere)
		USceneComponent* OurVisibleComponent;

	UPROPERTY(EditAnywhere)
		UCameraComponent* OurCamera;

	// Input functions
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void Move_XAxis(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void Move_YAxis(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void Sprint(bool NewRunning);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Sprint")
		bool IsRunning = false;

private:
	// This is for the sprint function and using the velocity.
		FVector CurrentVelocity;
	// use this for being able to move left and right well having the sprint function
		float DesiredForwardSpeed;
		float DesiredRightSpeed;
	// update the velocity everytime the button is pressed
		void UpdateVelocity();

};