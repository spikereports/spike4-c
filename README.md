# Spike Report

## Unreal Engine C++ player control


### Introduction

This spike will help learn and better understand the relationships between certain scripts/blueprints and how they work together to create a functional game.

### Goals

1. A small Unreal Engine project, built from scratch (can use content from any project from the Learn Tab in the Epic Games Launcher).
    * Any small game which you like, manage the scope yourself.
    * I would recommend finding a classic Arcade game that you want to copy.

1. The project must include the following custom classes (i.e. child classes):
    1. Game Mode (using Blueprint or C++)
    1. A Pawn or Character (C++)  
    1. A Player Controller (Blueprints or C++)
    1. A GameState (C++)

### Personnel

* Primary - Bradley
* Secondary - Vikram

### Technologies, Tools, and Resources used

1. [Unreal Engine API](https://docs.unrealengine.com/latest/INT/API/)

1. [Intro to Unreal C++](https://docs.unrealengine.com/latest/INT/Programming/Introduction/index.html)

1. [player input and pawns](https://docs.unrealengine.com/latest/INT/Programming/Tutorials/PlayerInput/index.html)
### Tasks Undertaken

1. Create a new blank c++ unreal project.

1. Create a pawn class that has basic movement functions
    1. set up the logic that is need for the pawn to work.
    1. create 3 functions that focus on movement
        1. Forward movement
        1. Right movement
        1. Sprint modifier
    1. A Uproperty bool for running state
    1. Create 3 variables and 1 function that focuses on Velocity
        1. Current Velocity
        1. Desired forward speed
        1. Desired right speed
        1. Update velocity
    1. Enable both Begin play and Tick properties
        1. Tick will update the pawns velocity every frame.
    1. Have logic for the movement functions
        1. Velocity and bool check for sprint

1. Create a Player Controller Class that called the pawns functions using the project settings inputs.
    1. Have the projects inputs call the pawns functions.

1. Create a game state that will count down from a number and uses begin play and tick floats.
    1. call tick function
    1. Begin play to call a onscreendebug message to view count down
    1. have the tick count down from number and call the game mode when it hits 0.

1. Create a game mode that will add functionality to the game.

1. Set up the Projects Game Mode in project settings, to the one just created and change the necessary default scripts to the scripts created.

### What we found out

1. The pawn class is where the logic is placed for how it should function but the player controller is where it accepts the inputs from the player to call said functions.

1. The difference between a game mode and game state. The game state focuses on what is currently happening within the game, and the game mode focuses on what should happen when something else is called.

1. If a coder isn't familiar with script coding, they will find this spike to be a challenge and will need help.